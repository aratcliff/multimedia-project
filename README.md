# Multimedia Project

A multimedia web based project completed in Year 12. Uses HTML, CSS, graphics created in Adobe Illustrator, and images edited in Adobe Photoshop

**NOTE:**

* There is minimal file structure
* There are few comments
* Site uses the same theme developed in the previous Year 12 Web project, the Albany Creek Cricket Club web site
    *  Locatable at: https://bitbucket.org/aratcliff/accc-site